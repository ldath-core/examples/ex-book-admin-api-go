# https://www.docker.com/blog/faster-multi-platform-builds-dockerfile-cross-compilation-guide/
FROM --platform=$BUILDPLATFORM golang:1.22-alpine AS builder
WORKDIR /go/src/gitlab.com/mobica-workshops/examples/go/gorilla/book-admin
COPY go.mod go.sum ./
RUN go mod download
ADD . ./
ARG TARGETOS
ARG TARGETARCH
RUN --mount=target=.:rw \
    --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
    GOOS=$TARGETOS GOARCH=$TARGETARCH go build -o book-admin .

FROM alpine:latest
RUN mkdir -p "/var/application"
COPY public /var/application/public
COPY --from=builder /go/src/gitlab.com/mobica-workshops/examples/go/gorilla/book-admin/book-admin /bin/book-admin
EXPOSE 8080
ENTRYPOINT ["/bin/book-admin"]
CMD ["serve", "--config", "/secrets/local.env.yaml", "-b", "0.0.0.0", "-p", "8080", "-c"]
