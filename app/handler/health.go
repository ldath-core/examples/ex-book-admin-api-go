package handler

import (
	"context"
	"fmt"
	"github.com/getsentry/sentry-go"
	"net/http"
	"time"

	"gitlab.com/mobica-workshops/examples/go/gorilla/book-admin/app/model"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// GetHealth will handle health get request
func (c *Controller) GetHealth(res http.ResponseWriter, _ *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	// Ping the primary
	m := true
	if err := c.MDB.Client().Ping(ctx, readpref.Primary()); err != nil {
		m = false
	}

	err := ResponseWriter(res, http.StatusOK, "book-admin api health", model.Health{
		Alive: true,
		Mongo: m,
	})
	if err != nil {
		c.Logger.Error(err)
	}
}

// GetError will handle error get request
func (c *Controller) GetError(w http.ResponseWriter, r *http.Request) {
	// Use GetHubFromContext to get a hub associated with the
	// current request. Hubs provide data isolation, such that tags,
	// breadcrumbs and other attributes are never mixed up across
	// requests.
	ctx := r.Context()
	hub := sentry.GetHubFromContext(ctx)

	hub.Scope().SetTag("url", r.URL.Path)
	hub.CaptureMessage("Error Test message")
	errors := make([]string, 0)
	errors = append(errors, "Error Test message")

	err := ErrorResponseWriter(w, http.StatusOK, "book-list api health", errors)
	if err != nil {
		c.Logger.Error(err)
	}
}

func (c *Controller) GetPanic(w http.ResponseWriter, r *http.Request) {
	var s []int
	_, err := fmt.Fprint(w, s[42]) // this line will panic
	if err != nil {
		c.Logger.Error(err)
	}
}
