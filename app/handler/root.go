package handler

import (
	"github.com/go-redis/redis/v8"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-admin/config"
	"go.mongodb.org/mongo-driver/mongo"
)

type Controller struct {
	MDB    *mongo.Database
	RDB    *redis.Client
	Logger *logrus.Logger
	Config *config.Config
}

func (c *Controller) NotFound(res http.ResponseWriter, req *http.Request) {
	c.Logger.Warningf("Not found: %s", req.RequestURI)
	err := ResponseWriter(res, http.StatusNotFound, "Page Not Found", nil)
	if err != nil {
		c.Logger.Error(err)
	}
}
