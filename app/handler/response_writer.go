package handler

import (
	"encoding/json"
	"net/http"

	"gitlab.com/mobica-workshops/examples/go/gorilla/book-admin/app/model"
)

// ResponseWriter will write result in http.ResponseWriter
func ErrorResponseWriter(res http.ResponseWriter, statusCode int, message string, errors []string) error {
	res.WriteHeader(statusCode)
	httpResponse := model.NewErrorResponse(statusCode, message, errors)
	err := json.NewEncoder(res).Encode(httpResponse)
	return err
}

// ResponseWriter will write result in http.ResponseWriter
func ResponseWriter(res http.ResponseWriter, statusCode int, message string, data interface{}) error {
	res.WriteHeader(statusCode)
	httpResponse := model.NewResponse(statusCode, message, data)
	err := json.NewEncoder(res).Encode(httpResponse)
	return err
}

// PaginatedResponseWriter will write result in http.ResponseWriter
func PaginatedResponseWriter(res http.ResponseWriter, statusCode int, count, skip, limit int64, message string, data interface{}) error {
	res.WriteHeader(statusCode)
	httpResponse := model.NewPaginatedResponse(statusCode, count, skip, limit, message, data)
	err := json.NewEncoder(res).Encode(httpResponse)
	return err
}
