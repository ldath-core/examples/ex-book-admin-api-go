package model

import (
	"bytes"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

// Admin is the data structure that we will save and receive.
type Admin struct {
	ID           primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Email        string             `json:"email" bson:"email,omitempty"`
	FirstName    string             `json:"firstName" bson:"firstName,omitempty"`
	LastName     string             `json:"lastName" bson:"lastName,omitempty"`
	PasswordHash string             `json:"passwordHash" bson:"passwordHash,omitempty"`
	Version      int                `json:"version" bson:"version,omitempty"`
}

type CreateAdminInput struct {
	Email     string `json:"email" validate:"email,required,omitempty"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Password  string `json:"password" validate:"required,omitempty"`
	Version   int    `json:"version"`
}

// NewAdmin will return an Admin{} instance, Admin structure factory function
func NewAdmin(email, firstName, lastName, password string) (*Admin, error) {
	byteArray, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		return nil, err
	}
	return &Admin{
		Email:        email,
		FirstName:    firstName,
		LastName:     lastName,
		PasswordHash: bytes.NewBuffer(byteArray).String(),
		Version:      1,
	}, nil
}
