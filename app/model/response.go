package model

type (
	// Response is the http json response schema
	Response struct {
		Status  int         `json:"status"`
		Message string      `json:"message"`
		Content interface{} `json:"content"`
	}

	ErrorResponse struct {
		Status  int      `json:"status"`
		Message string   `json:"message"`
		Errors  []string `json:"errors"`
	}

	// PaginatedContent is the paginated response json schema
	PaginatedContent struct {
		Count   int64       `json:"count"`
		Skip    int64       `json:"skip"`
		Limit   int64       `json:"limit"`
		Results interface{} `json:"results"`
	}

	// NextPrevContent is the paginated response json schema
	NextPrevContent struct {
		Count    int64       `json:"count,omitempty"`
		Next     string      `json:"next,omitempty"`
		Previous string      `json:"previous,omitempty"`
		Results  interface{} `json:"results"`
	}
)

// NewResponse is the Response struct factory function.
func NewResponse(status int, message string, content interface{}) *Response {
	return &Response{
		Status:  status,
		Message: message,
		Content: content,
	}
}

// NewErrorResponse is the ErrorResponse struct factory function.
func NewErrorResponse(status int, message string, errors []string) *ErrorResponse {
	return &ErrorResponse{
		Status:  status,
		Message: message,
		Errors:  errors,
	}
}

// NewPaginatedResponse will create http paginated response
func NewPaginatedResponse(status int, count, skip, limit int64, message string, results interface{}) *Response {
	return &Response{
		Status:  status,
		Message: message,
		Content: &PaginatedContent{
			Count:   count,
			Skip:    skip,
			Limit:   limit,
			Results: results,
		},
	}
}

// NewNextPrevResponse will create http paginated response based on next and prev
func NewNextPrevResponse(status int, count int64, message, next, prev string, results interface{}) *Response {
	return &Response{
		Status:  status,
		Message: message,
		Content: &NextPrevContent{
			Count:    count,
			Next:     next,
			Previous: prev,
			Results:  results,
		},
	}
}
