package model

type Health struct {
	Alive bool `json:"alive"`
	Mongo bool `json:"mongo"`
}
