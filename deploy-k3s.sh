#!/usr/bin/env bash
set -e
kubectx k3d-bookCluster
./build-multi-stage-container.sh
docker pull docker.io/bitnami/mongodb:4.4
k3d image import -c bookCluster book-admin:latest
k3d image import -c bookCluster docker.io/bitnami/mongodb:4.4
helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-book-admin ex-book/book-service --version 0.6.1 --dry-run --debug
helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-book-admin ex-book/book-service --version 0.6.1 --wait
