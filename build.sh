#!/usr/bin/env bash
set -e

source $(dirname "$0")/scripts/build_parse_args.sh

# Print build profile info
get_build_profile $@

# Collect building flags
profile_flag=$(get_build_flags $@)

# Set architecture variables
get_os_architecture $@

# Compile project
CGO_ENABLED=0 go build $profile_flag -o book-admin
