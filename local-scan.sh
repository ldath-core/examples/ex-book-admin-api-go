#!/usr/bin/env bash
set -e
./build-multi-stage-container.sh
trivy --version
# cache cleanup is needed when scanning images with the same tags, it does not remove the database
trivy image --clear-cache
# update vulnerabilities db
trivy image --download-db-only --no-progress --cache-dir .trivycache/
# Prints full report
trivy image --exit-code 1 --cache-dir .trivycache/ --severity CRITICAL,HIGH,MEDIUM --ignore-unfixed book-admin:latest
