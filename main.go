/*
Copyright © 2022 Artkadiusz Tułodziecki <atulodzi@gmail.com>
*/
package main

import "gitlab.com/mobica-workshops/examples/go/gorilla/book-admin/cmd"

func main() {
	cmd.Execute()
}
